pipeline {
    environment {
        TMP_CONTAINER = "tmp-${JOB_NAME}"
        TMP_IMG = "tmp-${JOB_NAME}-img"
        PROD_IMG = "prod-${JOB_NAME}-img"
        SITE = "http://ec2-3-248-220-229.eu-west-1.compute.amazonaws.com"
    }
    agent { label 'master' }
    stages {
        stage('Build') {
            steps {
                sh 'docker build -f Dockerfile.build -t ${TMP_IMG} .'
                sh 'docker create --name ${TMP_CONTAINER} ${TMP_IMG}'
                sh 'docker cp ${TMP_CONTAINER}:/tmp/ $PWD'
                sh 'docker container rm ${TMP_CONTAINER}'
            }
        }
        stage('Deploy') {
            steps {
                sh 'docker build -f Dockerfile.deploy -t ${PROD_IMG} .'
            }
        }
        stage('Run') {
            steps {
                sh 'docker ps -a -f name=${JOB_NAME} -q | xargs --no-run-if-empty docker container stop'
                sh 'docker container ls -a -fname=${JOB_NAME} -q | xargs -r docker container rm'
                sh 'docker run -d -p 80:80 --name ${JOB_NAME} ${PROD_IMG}'
            }
        }
        stage('Test') {
            steps {
                script {
                    int RESPONSE = sh( returnStdout: true, script: 'curl -o /dev/null -s -w "%{http_code}\n" ${SITE}')
                    if (RESPONSE != 200) {
                        sh 'docker ps -a -f name=${JOB_NAME} -q | xargs --no-run-if-empty docker container stop'
                        sh 'docker container ls -a -fname=${JOB_NAME} -q | xargs -r docker container rm'
                        sh 'docker rmi ${TMP_IMG}'
                        sh 'docker rmi ${PROD_IMG}'
                        sh 'rm -rf *'
                        error("Returned status code = $RESPONSE when calling $SITE")
                    }
                }
            }
        }
    }
}